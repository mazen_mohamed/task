<?php
require('classes.php');

$productsObject = new products();

// Check whether delete is triggered

if(isset($_POST['delete-product-btn'])) {

    if (isset($_POST['checkbox'])) {
        $IDs = $_POST['checkbox'];

        foreach ($IDs as $id) {
         $productsObject->deleteProducts($id);
        }
    }
}


$allProducts = $productsObject->getProducts();

?>
    <script>
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>

<?php
include('productList.phtml');