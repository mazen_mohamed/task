<?php

define('HOST','localhost');
define('USERNAME','root');
define('PASSWORD','');
define('DB_NAME','product');

abstract class main {
    private   $dbc;
    protected $SKU;
    protected $Name;
    protected $Price;
    protected $measurements;


    public function __construct(){

    $this->dbc = new PDO("mysql:host=".HOST.";dbname=".DB_NAME,USERNAME,PASSWORD);
    }

//  adding product to the database

    public function addProduct(){
            $SKU = $this->getSKU();
            $name = $this->getName();
            $price = $this->getPrice();
            $measurements = $this->getMeasurements();
            $stmt = $this->dbc->prepare("INSERT INTO `products`(`SKU`, `Name`, `Price`, `Measurements`) VALUES (:SKU,:name,:price,:measurements)");

            $stmt->bindParam('SKU',$SKU);
            $stmt->bindParam('name',$name);
            $stmt->bindParam('price',$price);
            $stmt->bindParam('measurements',$measurements);

            $stmt->execute();
    }

//  setters and getters

    public function setSKU($SKU){
        $this->SKU=$SKU;
    }
    public function setName($Name){
        $this->Name=$Name;
    }
    public function setPrice($Price){
        $this->Price=$Price;
    }

    abstract public function setMeasurements();

    public function getSKU(){
        return $this->SKU;
    }
    public function getName(){
        return $this->Name;
    }
    public function getPrice(){
        return $this->Price;
    }
    public function getMeasurements(){
        return $this->measurements  ;
    }

}

// Type switcher classes

class dvd extends main{
    public function productType(){

        return '<form>
                        <label class="label" for="size">Size (MB)</label>
                        <input class="input" id="size" type="number" name="size" min="0" required oninvalid="this.setCustomValidity(`Please, submit required data`)" oninput="this.setCustomValidity(``)"/>
                </form> '."</br>"."<div class=\"description\" >Please, provide size in MB</div>";
    }

    public function setMeasurements(){
        $description = 'Size: '.$_POST['size'].' MB';
        $this->measurements = $description;
    }

}

class furniture extends main{
    public function productType(){

        return '<form>
            <label class="label" for="height">Height (CM)</label>
            <input class="input" id="height" type="number" name="height" min="0" required oninvalid="this.setCustomValidity(`Please, submit required data`)" oninput="this.setCustomValidity(``)"/><br />
            <label class="label" for="width">Width (CM)</label>
            <input class="input" id="width" type="number" name="width"  min="0" required oninvalid="this.setCustomValidity(`Please, submit required data`)" oninput="this.setCustomValidity(``)" /><br />
            <label class="label" for="length">Length (CM)</label>
            <input class="input" id="length" type="number" name="length" min="0" required oninvalid="this.setCustomValidity(`Please, submit required data`)" oninput="this.setCustomValidity(``)" /><br />

        </form>'."</br>"."<div class=\"description\" >Please, provide dimensions in CM</div>";
    }

    public function setMeasurements(){
        $description = 'Dimension: '.$_POST['height'].'x'.$_POST['width'].'x'.$_POST['length'];
        $this->measurements = $description;
    }
}

class book extends main{
    public function productType(){

        return '<form>
            <label class="label" for="weight">Weight (KG)</label>
            <input class="input" id="weight" type="number" name="weight" min="0" required oninvalid="this.setCustomValidity(`Please, submit required data`)" oninput="this.setCustomValidity(``)"/>

        </form>'."</br>"."<div class=\"description\" >Please, provide weight in KG</div>";
    }

    public function setMeasurements(){
        $description = 'Weight: '.$_POST['weight'].' KG';
        $this->measurements = $description;
    }
}


//  Get and delete products logic

class products{
      private $dbc;
      private $id;
      private $SKU;
      private $Name;
      private $Price;
      private $Measurements;

    public function __construct(){

    $this->dbc = new PDO("mysql:host=".HOST.";dbname=".DB_NAME,USERNAME,PASSWORD);
    }

    public function getProducts(){

        $result = $this->dbc->query("SELECT * FROM `products`");
        $result->setFetchMode(PDO::FETCH_CLASS,"products");
        $result->execute();

        $products = array();
        while($row = $result->fetch()){
            $products[] = $row;
        }

        return $products;

    }

    public function getId(){
        return $this->id;
    }
    public function getSKU(){
        return $this->SKU;
    }
    public function getName(){
        return $this->Name;
    }
    public function getPrice(){
        return $this->Price;
    }
    public function getMeasurements(){
        return $this->Measurements  ;
    }


    public function deleteProducts($id){

        $stmt = $this->dbc->prepare("DELETE FROM `products` WHERE `id`=:id");
        $stmt->bindParam('id',$id);
        $stmt->execute();

    }

    public function __deconstruct(){
        $this->dbc= null;

    }
}



