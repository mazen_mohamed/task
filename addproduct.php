<?php

require('classes.php');

// Check whether add product button is triggered

if(isset($_POST['SKU'])){

    $product = new $_POST['select']();

    $product->setSKU($_POST['SKU']);
    $product->setName($_POST['name']);
    $product->setPrice($_POST['price']);
    $product->setMeasurements();
    $product->addProduct();
    ?>
    <Script>
        window.location='index';
    </Script>
    <?php
}

include('addProductTemplate.html');

?>

